<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use DB;

class CreatecontactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view("create");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $contact = new contact();

      $contact->Nom = $request->input('Nom');
      $contact->Prenom = $request->input('Prenom');
      $contact->Numberphone = $request->input('Numberphone');
      $contact->Email = $request->input('Email');

      $contact->save();

      return redirect('list');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\createcontact  $createcontact
     * @return \Illuminate\Http\Response
     */
    public function show(createcontact $createcontact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\createcontact  $createcontact
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $contacts = Contacts::find($id);
      return view('editcontact', ['contact' => $contact]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\createcontact  $createcontact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, createcontact $createcontact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\createcontact  $createcontact
     * @return \Illuminate\Http\Response
     */
    public function destroy(createcontact $createcontact)
    {
        //
    }
}
