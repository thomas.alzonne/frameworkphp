<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\contact;
use Faker\Generator as Faker;

$factory->define(contact::class, function (Faker $faker) {
    return [
        'Nom' => $faker->Name,
        'Prenom' => $faker->Name,
        'Numberphone' => $faker->phoneNumber,
        'Email' => $faker->email,
    ];
});
