@extends("templates/main")
@section("titre")
Create Contact
@endsection

@section("nom")
Thomas Alzonne
@endsection

@section("content")
<form action="/create" method="POST">
  @csrf
<div class="form-group">
<label for="title">Nom</label>
<input type="text" class="form-control" name="Nom"  placeholder="Votre Nom">
</div>
<div class="form-group">
<label for="date_debut">Prenom</label>
<input type="text" class="form-control" name="Prenom"  placeholder="Votre Prénom">
</div>
<div class="form-group">
<label for="date_fin">Numéro de téléphone</label>
<input type="text" class="form-control" name="Numberphone"  placeholder="Votre numéro de téléphone">
</div>
<div class="form-group">
<label for="exampleInputEmail1">Email address</label>
<input type="email" name="Email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Votre Email">
</div>
<button type="submit" name="send" class="btn btn-primary">Submit</button>
</form>
@endsection
