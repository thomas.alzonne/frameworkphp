@extends("templates/main")
@section("titre")
Contact List
@endsection

@section("nom")
Thomas Alzonne
@endsection

@section("content")
<table class="table table-striped table-dark">
<thead>
  <tr>
    <th scope="col">#</th>
    <th scope="col">Nom</th>
    <th scope="col">Prenom</th>
    <th scope="col">Numberphone</th>
    <th scope="col">Email</th>
    <th scope="col">Modifier</th>
    <th scope="col">Supprimer</th>
  </tr>
</thead>
<tbody>
  @foreach($contacts as $contact)
  <tr>
    <td>{{$contact->id}}</td>
    <td>{{$contact->Prenom}}</td>
    <td>{{$contact->Nom}}</td>
    <td>{{$contact->Numberphone}}</td>
    <td>{{$contact->Email}}</td>
    <td><a href="/editcontact/{{$contact->id}}"><img src="images/document.png"></a></td>
    <td><a href=""><img src="images/bin.png"></a></td>
  </tr>
  @endforeach
</tbody>
</table>
@endsection
