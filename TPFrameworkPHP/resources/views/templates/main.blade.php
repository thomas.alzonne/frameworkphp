<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/axentix@0.3.1/dist/css/axentix.min.css">
    <title>@yield("titre")</title>
  </head>
  <nav class="navbar cyan">
    <a href="#" target="_blank" class="nav-brand">@yield("nom")</a>
    <ul class="right">
      <li><a href="/index">Accueil</a></li>
      <li><a href="/list">Liste des contacts</a></li>
      <li><a href="/create">Add Contact</a></li>
    </ul>
  </nav>
  <body>
    @yield("content")
  </body>
</html>
